﻿using System;

namespace DoublyLinkedListLibrary
{
    public class DoublyLinkedList<T>
    {
        private Item<T>? Head { get; set; }
        public Item<T>? Tail { get; private set; }

        public void Add(T value)
        {
            if (Head == null)
            {
                Tail = Head = new Item<T> {Value = value, Index = 0, FrontItem = null, PerItem = null};
            }
            else
            {
                Item<T> newItem = new() {Value = value, Index = Tail.Index + 1, FrontItem = Tail, PerItem = null};
                Tail.PerItem = newItem;
                Tail = newItem;
            }
        }

        public Item<T> SearchItem(int index)
        {
            if (Head != null && Head.Index == index)
            {
                return Head;
            }
            else if (Head.PerItem != null)
            {
                return IterationOverItem(Head.PerItem, index);
            }

            throw new Exception("Uncorrect index");
        }

        private Item<T> NeedItem { get; set; }

        private Item<T> IterationOverItem(Item<T> item, int index) //перебор элементов, пока не найдем с нужным индексом
        {
            if (item.Index == index)
            {
                NeedItem = item;
            }
            else if (item.Index != index)
            {
                if (item.PerItem != null)
                {
                    IterationOverItem(item.PerItem, index);
                }
                else
                {
                    throw new Exception("Uncorrect index");
                }
            }

            return NeedItem;
        }

        public void Insert(int index, T value) //вставить элемент
        {
            Item<T> originalItem = SearchItem(index);
            if (index != 0)
            {
                Item<T> newItem = new()
                    {Value = value, Index = index, FrontItem = originalItem.FrontItem, PerItem = originalItem};
                originalItem.FrontItem.PerItem = newItem;
                originalItem.FrontItem = newItem;
            }
            else if (index == 0)
            {
                Item<T> newItem = new() {Value = value, Index = index, FrontItem = null, PerItem = originalItem};
                Head.FrontItem = newItem;
                Head = newItem;
            }

            ChangeIndex(originalItem, '+');
        }

        private void ChangeIndex(Item<T> item, char op)
        {
            if (op == '+')
            {
                item.Index += 1;
            }

            if (op == '-')
            {
                item.Index -= 1;
            }

            if (item.PerItem != null)
            {
                ChangeIndex(item.PerItem, op);
            }
        }

        public void RemoveAt(int index) //удалить элемент
        {
            Item<T> item = SearchItem(index);
            if (Head == item && Tail == item)
            {
                Head = Tail = null;
            }
            else if (Head == item)
            {
                Head = item.PerItem;
                if (Head != null)
                {
                    Head.FrontItem = null;
                    ChangeIndex(Head, '-');
                }
            }
            else if (Tail == item)
            {
                Tail = item.FrontItem;
                Tail.PerItem = null;
            }
            else if (Head != item && Tail != item)
            {
                (item.FrontItem, item.PerItem) = (item.PerItem, item.FrontItem);
            }
        }

        public int Count()
        {
            if (Tail == null)
                return 0;
            return Tail.Index + 1;
        }
    }

    public class Item<T>
    {
        public T Value { get; set; }
        public Item<T>? FrontItem { get; set; } //перед ним
        public Item<T>? PerItem { get; set; } //за ним
        public int Index { get; set; }
    }
}