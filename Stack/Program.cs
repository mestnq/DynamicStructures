﻿using System;
using DoublyLinkedListLibrary;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(4);
            stack.Push(3);
            stack.Push(2);
            stack.Push(1);
            stack.Push(0);
            stack.Pop();
            stack.Pop();
            stack.Pop();
            stack.Pop();
            stack.Top();
            stack.Pop();
            stack.IsEmpty();
            stack.Push(100);
            stack.Push(200);
            stack.Print();
        }
    }

    class Stack<T> //стек
    {
        private static readonly DoublyLinkedList<T> DoublyLinkedList = new();

        public void Push(T value) //добавляет элемент в стек на первое место
        {
            if (DoublyLinkedList.Count() != 0)
            {
                DoublyLinkedList.Insert(0, value);
            }
            else if (DoublyLinkedList.Count() == 0)
                DoublyLinkedList.Add(value);
        }

        public T Pop() //извлекает и возвращает первый элемент из стека
        {
            T top = DoublyLinkedList.SearchItem(0).Value;
            DoublyLinkedList.RemoveAt(0);
            return top;
        }

        public T Top() //просто возвращает первый элемент из стека без его удаления
        {
            return DoublyLinkedList.SearchItem(0).Value;
        }

        public bool IsEmpty() //определяет пуст ли стек
        {
            return DoublyLinkedList.Count() == 0;
        }

        public void Print() //выводит все что есть в стеке
        {
            if (DoublyLinkedList.Tail != null)
            {
                for (int i = 0; i <= DoublyLinkedList.Tail.Index; i++)
                {
                    Item<T> item = DoublyLinkedList.SearchItem(i);
                    Console.WriteLine(item.Value);
                }
            }
            else
                throw new Exception("Stack is empty");
        }
    }
}