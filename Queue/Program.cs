﻿using System;
using DoublyLinkedListLibrary;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> queue = new Queue<int>();
            queue.Enqueue(0);
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Dequeue();
            queue.Top();
            queue.Dequeue();
            queue.Dequeue();
            queue.Dequeue();
            queue.Dequeue();
            queue.IsEmpty();
            queue.Enqueue(100);
            queue.Enqueue(200);
            queue.Dequeue();
            queue.Dequeue();
            queue.Print();
        }
    }

    class Queue<T>
    {
        private static readonly DoublyLinkedList<T> DoublyLinkedList = new();

        public void Enqueue(T value) // добавляет элемент в очередь
        {
            DoublyLinkedList.Add(value);
        }

        public T Dequeue() // извлекает и возвращает первый элемент очереди
        {
            T top = DoublyLinkedList.SearchItem(0).Value;
            DoublyLinkedList.RemoveAt(0);
            return top;
        }

        public T Top() // просмотр первого элемента очереди
        {
            return DoublyLinkedList.SearchItem(0).Value;
        }

        public bool IsEmpty() // определяет пуста ли очередь
        {
            return DoublyLinkedList.Count() == 0;
        }

        public void Print() // выводит все что есть в очереди
        {
            if (DoublyLinkedList.Tail != null)
            {
                for (int i = 0; i <= DoublyLinkedList.Tail.Index; i++)
                {
                    Item<T> item = DoublyLinkedList.SearchItem(i);
                    Console.WriteLine(item.Value);
                }
            }
            else
                throw new Exception("Queue is empty");
        }
    }
}